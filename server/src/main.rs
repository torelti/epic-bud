use mlua::{ Function, Lua, Result};
use std::fs;
fn main() -> Result<()> {
    let lua = Lua::new();
    let globals = lua.globals();

    globals.set("string_var", "hello")?;
    globals.set("int_var", 42)?;

    let print: Function = globals.get("print")?;
    print.call::<_, ()>("hello from rust")?;

    //load file into string
    let file = fs::read_to_string("server/src/lua/printtest.lua")?;
    //load string into lua
    lua.load(&file).exec()?;

    Ok(())
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_server() {
        // assert_eq!(globals.get::<_, String>("string_var")?, "hello");
        // assert_eq!(globals.get::<_, i64>("int_var")?, 42);
    }
}
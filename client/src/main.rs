use building_blocks::{
    core::sdfu::{Sphere, SDF},
    mesh::{surface_nets, SurfaceNetsBuffer},
    prelude::*,
};

#[macro_use]
extern crate glium;

use glium::{
    glutin::{
        self,
        event::{ElementState, KeyboardInput, VirtualKeyCode},
    },
    IndexBuffer, Program, Surface, VertexBuffer,
};

static mut CAMERA_ANGLE: f32 = std::f32::consts::PI / 2.0;
static mut CAMERA_POSITION: [f32; 3] = [0.0, 0.0, -5.0];

const POSITION_DELTA: f32 = 0.05;
const ANGLE_DELTA: f32 = 0.001;

static mut W_PRESSED: bool = false;
static mut A_PRESSED: bool = false;
static mut S_PRESSED: bool = false;
static mut D_PRESSED: bool = false;

fn main() {
    let center = Point3f::fill(25.0);
    let radius = 10.0;
    let sphere_sdf = Sphere::new(radius).translate(center);

    let extent = Extent3i::from_min_and_shape(Point3i::ZERO, Point3i::fill(50));
    let samples = Array3x1::fill_with(extent, |p| sphere_sdf.dist(Point3f::from(p)));

    let mut mesh_buffer = SurfaceNetsBuffer::default();
    let voxel_size = 2.0; // length of the edge of a voxel
    surface_nets(&samples, samples.extent(), voxel_size, &mut mesh_buffer);

    let mesh = mesh_buffer.mesh;
    let num_verts = mesh.positions.len();

    // building the display, ie. the main object
    let event_loop = glutin::event_loop::EventLoop::new();
    let wb = glutin::window::WindowBuilder::new();
    let cb = glutin::ContextBuilder::new().with_depth_buffer(24);
    let display = glium::Display::new(wb, cb, &event_loop).unwrap();

    // load b.b. mesh as glium mesh

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 3],
        normal: [f32; 3],
    }

    implement_vertex!(Vertex, position, normal);

    let mut vertex_data = Vec::new();
    for i in 0..num_verts {
        vertex_data.push(Vertex {
            position: mesh.positions[i],
            normal: mesh.normals[i],
        })
    }

    let vertex_buffer = VertexBuffer::new(&display, &vertex_data).unwrap();
    let indices = IndexBuffer::new(
        &display,
        glium::index::PrimitiveType::TrianglesList,
        &mesh.indices,
    )
    .unwrap();

    // the program
    let vertex_src = include_str!("./shaders/test.vert");
    let fragment_src = include_str!("./shaders/test.frag");
    let program = Program::from_source(&display, vertex_src, fragment_src, None).unwrap();

    event_loop.run(move |event, _, control_flow| {
        let next_frame_time =
            std::time::Instant::now() + std::time::Duration::from_nanos(16_666_667);
        *control_flow = glutin::event_loop::ControlFlow::WaitUntil(next_frame_time);

        match event {
            glutin::event::Event::WindowEvent { event, .. } => match event {
                glutin::event::WindowEvent::CloseRequested => {
                    *control_flow = glutin::event_loop::ControlFlow::Exit;
                    return;
                }
                glutin::event::WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            virtual_keycode: Some(virtual_keycode),
                            state,
                            ..
                        },
                    ..
                } => {
                    unsafe {
                        match virtual_keycode {
                            VirtualKeyCode::W => update_key_globals(&mut W_PRESSED, state),
                            VirtualKeyCode::A => update_key_globals(&mut A_PRESSED, state),
                            VirtualKeyCode::S => update_key_globals(&mut S_PRESSED, state),
                            VirtualKeyCode::D => update_key_globals(&mut D_PRESSED, state),
                            _ => {}
                        }
                    }
                }
                _ => {},
            },
            glutin::event::Event::NewEvents(cause) => match cause {
                glutin::event::StartCause::ResumeTimeReached { .. } => (),
                glutin::event::StartCause::Init => (),
                _ => {},
            },
            _ => {},
        }

        update_camera_state();

        let mut target = display.draw();
        target.clear_color_and_depth((0.0, 0.0, 0.1, 1.0), 1.0);

        let model = [
            [0.01, 0.0, 0.0, 0.0],
            [0.0, 0.01, 0.0, 0.0],
            [0.0, 0.0, 0.01, 0.0],
            [0.0, 0.0, 0.0, 1.0f32],
        ];

        let view = view_matrix_from_camera_state();

        let perspective = {
            let (width, height) = target.get_dimensions();
            let aspect_ratio = height as f32 / width as f32;

            let fov: f32 = std::f32::consts::PI / 3.0;
            let zfar = 1024.0;
            let znear = 0.1;

            let f = 1.0 / (fov / 2.0).tan();

            [
                [f * aspect_ratio, 0.0, 0.0, 0.0],
                [0.0, f, 0.0, 0.0],
                [0.0, 0.0, (zfar + znear) / (zfar - znear), 1.0],
                [0.0, 0.0, -(2.0 * zfar * znear) / (zfar - znear), 0.0],
            ]
        };

        let light = [-1.0, 0.4, 0.9f32];

        let params = glium::DrawParameters {
            depth: glium::Depth {
                test: glium::draw_parameters::DepthTest::IfLess,
                write: true,
                ..Default::default()
            },
            ..Default::default()
        };

        target
            .draw(
                &vertex_buffer,
                &indices,
                &program,
                &uniform! { model: model, view: view, perspective: perspective, u_light: light },
                &params,
            )
            .unwrap();
        target.finish().unwrap();
    });
}

fn update_key_globals(state: &mut bool, key_state: ElementState) {
    match key_state {
        ElementState::Pressed => *state = true,
        ElementState::Released => *state = false,
    }
}

fn update_camera_state() {
    unsafe {
        if W_PRESSED {
            CAMERA_POSITION[0] += POSITION_DELTA * CAMERA_ANGLE.cos();
            CAMERA_POSITION[2] += POSITION_DELTA * CAMERA_ANGLE.sin();
        }
        if S_PRESSED {
            CAMERA_POSITION[0] -= POSITION_DELTA * CAMERA_ANGLE.cos();
            CAMERA_POSITION[2] -= POSITION_DELTA * CAMERA_ANGLE.sin();
        }
        if A_PRESSED {
            CAMERA_ANGLE += ANGLE_DELTA;
        }
        if D_PRESSED {
            CAMERA_ANGLE -= ANGLE_DELTA;
        }
    }
}

fn view_matrix_from_camera_state() -> [[f32; 4]; 4] {
    unsafe {
        let direction = [CAMERA_ANGLE.cos(), 0.0, CAMERA_ANGLE.sin()];
        view_matrix(&CAMERA_POSITION, &direction, &[0.0f32, 1.0f32, 0.0f32])
    }
}

fn view_matrix(position: &[f32; 3], direction: &[f32; 3], up: &[f32; 3]) -> [[f32; 4]; 4] {
    let f = {
        let f = direction;
        let len = f[0] * f[0] + f[1] * f[1] + f[2] * f[2];
        let len = len.sqrt();
        [f[0] / len, f[1] / len, f[2] / len]
    };

    let s = [
        up[1] * f[2] - up[2] * f[1],
        up[2] * f[0] - up[0] * f[2],
        up[0] * f[1] - up[1] * f[0],
    ];

    let s_norm = {
        let len = s[0] * s[0] + s[1] * s[1] + s[2] * s[2];
        let len = len.sqrt();
        [s[0] / len, s[1] / len, s[2] / len]
    };

    let u = [
        f[1] * s_norm[2] - f[2] * s_norm[1],
        f[2] * s_norm[0] - f[0] * s_norm[2],
        f[0] * s_norm[1] - f[1] * s_norm[0],
    ];

    let p = [
        -position[0] * s_norm[0] - position[1] * s_norm[1] - position[2] * s_norm[2],
        -position[0] * u[0] - position[1] * u[1] - position[2] * u[2],
        -position[0] * f[0] - position[1] * f[1] - position[2] * f[2],
    ];

    [
        [s_norm[0], u[0], f[0], 0.0],
        [s_norm[1], u[1], f[1], 0.0],
        [s_norm[2], u[2], f[2], 0.0],
        [p[0], p[1], p[2], 1.0],
    ]
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_client() {
        assert_eq!(3 + 3, 6);
    }
}

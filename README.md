# Epic Buds Incorporated

# Pre-commit hooks
After cloning the repo, run `scripts/install_hooks` in the toplevel working
directory (may have to chmod first). If you need to change the hooks, edit
`scripts/run_tests`. To commit without running hooks, run
`git commit -m "..." --no-verify`. `--no-verify` can also apply to amending
commits.

- stay hungry
- stay real
- stay pious

![Epic Bud](https://cdn.discordapp.com/attachments/908630833773027359/916571691747016794/9dc0728a870a657f1669e3e8ee6cb8ca.png)